# coding=utf-8
__author__ = 'andy'

import requests
import time
from utils import get_md5
from utils import post_to_es
from datetime import datetime
from  utils import choice_useragent

name = 'search_audiovisual_toutiao'
qlist = []
index_name = post_to_es(name, qlist)[0]

queryWord = '北大'
# 头条
offset = 0

# 头条api
url = 'https://www.toutiao.com/search_content/?offset={}&format=json&keyword={}&autoload=true&count=20&cur_tab=2'.format(
    offset, queryWord)

headers = {'User-Agent': choice_useragent()}


def get_vedio_url(url, headers=headers):
    try:
        r = requests.get(url, timeout=1)
        for i in r.json()['data']:
            print(222)
            print(i)
            item = {}
            item['program_name'] = i.get('title') if i.get('title') else ''

            publish_time = time.mktime(time.strptime(i.get('datetime'), "%Y-%m-%d %H:%M")) if i.get(
                'datetime') else  datetime.now().strftime("%Y-%m-%d %H:%M")

            item['publish_time'] = publish_time
            item['create_time'] = i.get('create_time') if i.get('create_time') else str(int(time.time()))
            item['video_url'] = i.get('share_url') if i.get('share_url') else ''
            item['imgurl'] = "http:" + i.get('image_url') if i.get('image_url') else ''
            item['source'] = i.get('source') if i.get('source') else ''
            item['duration'] = i.get('video_duration_str') if i.get('video_duration_str') else ''
            item['spider_time'] = str(int(time.time()))
            qlist.append({
                "_index": index_name,
                "_type": "audiovisual_outsite",
                # "_id": time.time(),
                # url转md5 实现去重
                "_id": get_md5(i['videoUrl']),
                "_source": item})

        if r.json()['return_count'] > 10:

            global offset
            if offset < 60:
                offset += 20

                url1 = 'https://www.toutiao.com/search_content/?offset={}&format=json&keyword={}&autoload=true&count=20&cur_tab=2'.format(
                    offset, queryWord)

                return get_vedio_url(url1)

    except Exception as e:
        print(e)

    post_to_es(name, qlist)


def toutiao_start():
    print(get_vedio_url(url))
    print(len(qlist))


if __name__ == '__main__':
    toutiao_start()
