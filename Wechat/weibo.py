# coding=utf-8
__author__ = 'andy'

import requests
from bs4 import BeautifulSoup
import re
import arrow
import time
import datetime
from utils import get_md5
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from gevent import monkey
monkey.patch_all()
from gevent.pool import Pool
from utils import  get_word_from_redis
from utils import choice_cookie
from  utils import post_to_es
from utils import choice_useragent

name = 'search_audiovisual_weibo'
qlist = []

url = 'https://weibo.cn/search/'
pageno = 1
endtime = str(arrow.now().date()).replace('-', '')
#queryWord = get_word_from_redis
queryWord = '苹果'

index_name = post_to_es(name, qlist)[0]

r = requests.get('http://127.0.0.1:5000/weibo/random')
Cookie = choice_cookie()

headers = {"Host": "weibo.cn",

           "User-Agent": choice_useragent(),
           "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
           "Referer": "https://weibo.cn/search/mblog?advanced=mblog&f=s",
           "Accept-Encoding": "gzip, deflate, br",
           "Accept-Language": "en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4",
           "Cookie": Cookie,
           "Content-Length": "121"}

# 获取链接
total_url = []

#不换cookie可能返回为空
def get_total_url():
    for i in range(2):
        data = {'advancedfilter': 1, 'keyword': queryWord, 'hasvideo': 1, 'haslink': 1, 'nick': '',
                'starttime': '', 'endtime': endtime, 'sort': 'time', 'smblog': "搜索", "page": pageno * i}
        print(data)
        try:
            r = requests.post(url, data=data, headers=headers, timeout=1, verify=False)
            print(r.status_code)
            h = re.findall(r'<a href=[^<]+转发.*?</a>', r.text)
            for i in h:
                i = i.replace('amp;', '')
                a = re.findall(r'(https://[^"]+)"', i)[0]
                print(a)
                total_url.append(a)

        except Exception as e:
            print(e)
    return total_url


def get_video_url(url):

    try:
        r = requests.get(url, timeout=2, verify=False)
        a = re.findall(r'<a href="([^<]+)">(\S+的\S*视频)</a>', r.text)
        print(a)
        b = list(a[0])[0].replace('amp;', '')

        c = a[0][1]
        res = requests.get(b,timeout=2)
        title = re.findall(r'(?is)<title.*?>([^<]+?)</title>', res.text)[0]
        print(title)
        item = {}

        item['program_name'] = title
        item['source'] = c
        item['video_url'] = b
        item['imgurl'] = ''

        item['duration'] = ''
        item['publish_time'] = str(int(time.time()))
        item['spider_time'] = str(int(time.time()))
        item['create_time'] = str(int(time.time()))
        item['tag'] = queryWord
        qlist.append({
            "_index": index_name,
            "_type": "audiovisual_outsite",
            # "_id": time.time(),
            # url转md5 实现去重
            "_id": get_md5(item['video_url']),
            "_source": item})

    except Exception as e:
        print(e)
    post_to_es(name, qlist)



# def get_videourl(url):
#     qlist = []
#     try:
#         r = requests.get(url,timeout=2,verify=False)
#         soup = BeautifulSoup(r.text, 'lxml')
#
#         video_url = re.findall(r'"videoSrc":"([^"]+)"', r.text)[0] if re.findall(r'"videoSrc":"([^"]+)"',r.text) else ''
#         if video_url:
#             print(video_url)
#             item = {}
#             item['title'] = soup.select('title')[0].get_text()
#             item['duration'] = ''
#             item['source'] = soup.select('.personalDataN')[0].get_text() if soup.select('.personalDataN') else ''
#
#             item['video_url'] = video_url
#             item['imgurl'] = re.findall(r'poster":"([^"]+)"', r.text)[0] if re.findall(r'poster":"([^"]+)"',
#                                                                                        r.text) else ''
#             pub = re.findall(r'<span>(\d{2}-\d{2})</span>', r.text)[0] if re.findall(r'<span>(\d{2}-\d{2})</span>',
#                                                                                      r.text) else ''
#             print(pub)
#             try:
#                 item['publish_time'] = str(
#                     int(time.mktime(time.strptime(str(arrow.now().year) + '-' + pub, '%Y-%m-%d'))))
#             except Exception as e:
#                 item['publish_time'] = str(int(time.time()))
#             item['spider_time'] = str(int(time.time()))
#             item['create_time'] = str(int(time.time()))
#             item['tag'] = queryWord
#             qlist.append({
#                 "_index": index_name,
#                 "_type": "audiovisual_outsite",
#                 # "_id": time.time(),
#                 # url转md5 实现去重
#                 "_id": get_md5(item['video_url']),
#                 "_source": item})
#         helpers.bulk(es, qlist)
#     except:
#         print('request timeout1')




def weibo_start():
    pool = Pool(6)
    print("微博开始了")
    pool.map(get_video_url, get_total_url())
    print(qlist)

if __name__ == "__main__":
   print(get_total_url())
