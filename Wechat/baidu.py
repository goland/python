#coding=utf-8
__author__ = 'andy'

import requests
from utils import post_to_es
from utils import get_md5
import time
from gevent import monkey
from utils import  get_word_from_redis
from  utils import choice_useragent
monkey.patch_all()
from gevent.pool import Pool

headers = {'User-Agent': choice_useragent()}
# 百度参数

pn = 50
rn = 50
order = 1
queryWord = get_word_from_redis()

# 百度api
url = 'http://app.video.baidu.com/app?word={}%&pn={}&rn={}&order={}'.format(queryWord, pn, rn, order)

print(url)



name = 'search_audiovisual_baidu'
qlist = []
index_name = post_to_es(name,qlist)[0]



# json 一次取一页只能取50个
# 百度接口

total_url = []

def get_total_url():
    r = requests.get(url)
    # 总个数
    dispnum = int(r.json()['dispnum'])
    if dispnum <= 50:
        total_url.append(url)

    if dispnum < 10000:

        pages = int(dispnum / 50 + 1)
        print(pages)
        for i in range(pages):
            url1 = 'http://app.video.baidu.com/app?word={}%&pn={}&rn={}&order={}'.format(queryWord, pn * i, rn, order)
            total_url.append(url1)

    if dispnum > 10000:
        for i in range(10):
            url2 = 'http://app.video.baidu.com/app?word={}%&pn={}&rn={}&order={}'.format(queryWord, pn * i, rn, order)
            total_url.append(url2)
    return total_url


def get_video_url(url):
    group_list = []
    r1 = requests.get(url,headers=headers,verify=False)

    if 'result' not in r1.json().keys():
        return None
    else:

        b = r1.json()['result']

        print(len(b))

        for k, i in enumerate(b):
            print(i)

            titleUrl = i['titleUrl']

            restitle = i['restitle']
            # print(titleUrl,restitle)
            item = {}
            item['program_name'] = i['restitle']
            item['source'] = i['srcShortUrl']
            item['video_url'] = i['videoUrl']
            item['imgurl'] = i['imgUrl']

            item['duration'] = i['duration']

            item['spider_time'] = str(int(time.time()))
            item['publish_time'] = str(int(time.time()))
            item['create_time'] = str(int(time.time()))
            item['tag'] = queryWord
            try:
                r2 = requests.get(titleUrl, timeout=1,)
                r2_status = r2.status_code

                if r2_status == 200:
                    # if restitle.rstrip('...') in r.text:
                    #     print('true')
                    group_list.append({
                        "_index": index_name,
                        "_type": "audiovisual_outsite",
                        # "_id": time.time(),
                        # url转md5 实现去重
                        "_id": get_md5(i['videoUrl']),
                        "_source": item})
                    # else:
                    #     print('不匹配')
                else:
                    print('网页异常')
            except Exception as e:
                print(e)
    post_to_es(name, group_list)
    return group_list



def baidu_start():
    print("百度开始了")
    pool = Pool(6)
    pool.map(get_video_url, get_total_url())
if __name__ == '__main__':
    baidu_start()

