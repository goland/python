#coding=utf-8
__author__ = 'andy'

import requests
from datetime import datetime

import re
import time
from gevent import monkey
monkey.patch_all()
from gevent.pool import Pool
from  utils import post_to_es
from utils import get_md5,get_word_from_redis
from utils import choice_useragent
import re

# r = requests.get('http://47.93.30.165:8000/')
# a =r.text
# proxies = { "http": "http://"+a}
# print(proxies)
headers = {
    "Host": "weixin.sogou.com",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": choice_useragent(),
    "Referer": "http://weixin.sogou.com/weixin?type=2&ie=utf8&query=%E5%8F%B0%E9%A3%8E",
    "Accept-Encoding": "gzip, deflate, sdch",
    "Accept-Language": "zh-CN,zh;q=0.8",
    "Cookie": "SUID=9BF2DFDD232C940A00000000590F3995; SUV=003F7A5A6FC7BB37590F399659338926; CXID=FF77CD6E7DC6CB0C8408DB98712D33DF; SUID=37BBC76F5412940A00000000590F3995; ABTEST=8|1505206661|v1; SNUID=40C13A3A46431C196E8FB54646009FE9; IPLOC=CN1100; weixinIndexVisited=1; JSESSIONID=aaa7SPXtAWYRPhgHagz6v; ppinf=5|1506753997|1507963597|dHJ1c3Q6MToxfGNsaWVudGlkOjQ6MjAxN3x1bmlxbmFtZToxODolRTUlQTQlQTklRTglQTElOEN8Y3J0OjEwOjE1MDY3NTM5OTd8cmVmbmljazoxODolRTUlQTQlQTklRTglQTElOEN8dXNlcmlkOjQ0Om85dDJsdUJqTTIxQXlJWlM3bTY3QWlESjVrWW9Ad2VpeGluLnNvaHUuY29tfA; pprdig=AY3rgUWbFYXaUztZw7JCCdWbZ9RmWggGyZFGcK7437_rCCMVQMb91ECgwGeDUHNMwCwQhc9zR2aXFUpWp0_XjaRiCEc7gcniE0SualZnDvZ3SQUFJjY_6gnl30z4BG3BbtWsoNVtSZKIvun7rOFb5BLh1cJtL6uIrzE6MqXq8YQ; sgid=31-28667175-AVnPPc0YU9Jfzvib66ZjLZ1U; ppmdig=150675404100000074304d73b8ff593153bc12d2ec6d3f38; sct=3"}


queryWord = get_word_from_redis()
#queryWord = "苹果"
page = 1

total_url = []
name = 'search_audiovisual_weixin'

qlist = []
index_name = post_to_es(name,qlist)[0]

def get_total_url():
    for i in range(1, 3):

        url = "http://weixin.sogou.com/weixin?usip=&query={}=&tsn=0&et=&interation=458756&type=2&wxid=&page={}&ie=utf8".format(
            queryWord, page * i)
        r = requests.get(url, headers=headers, timeout=1)

        r.encoding = 'utf-8'


        a = re.findall(r'data-share="([^"]+)"', r.text)

        for j in a:
            j = j.replace('amp;', '')
            total_url.append(j)
    return total_url

def get_video_url(url):

    try:
        r = requests.get(url,timeout=2)

        title = re.findall(r'(?is)<title.*?>([^<]+?)</title>', r.text)

        date = re.findall(r'(\d{4}-\d{2}-\d{2})', r.text)[0] if re.findall(r'(\d{4}-\d{2}-\d{2})', r.text) else str(datetime.now().date())
        date1 = time.mktime(time.strptime(date, "%Y-%m-%d"))
        source = re.findall(r'<a [\s\S]*id="post-user">(.+?)</a>', r.text) if re.findall(r'<a [\s\S]*id="post-user">(.+?)</a>', r.text) else ''

        item ={}
        item['program_name'] = title
        item['source'] = source
        item['video_url'] = url
        item['imgurl'] = ''
        item['duration'] = ''
        item['publish_time'] = str(int(date1))
        item['spider_time'] = str(int(time.time()))
        item['create_time'] = str(int(time.time()))
        item['tag'] = queryWord
        qlist.append({
            "_index": index_name,
            "_type": "audiovisual_outsite",
            # "_id": time.time(),
            # url转md5 实现去重
            "_id": get_md5(item['video_url']),
            "_source": item})
    except Exception as e:
        print(e)
    post_to_es(name, qlist)




def wechat_start():
    print("微信开始了")
    pool = Pool(6)
    #print(get_total_url())
    pool.map(get_video_url, get_total_url())
    print(qlist)


if __name__ == '__main__':
    wechat_start()