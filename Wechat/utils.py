#coding=utf-8
__author__ = 'andy'

import redis
import datetime
import hashlib
import re
import sys
import random
import requests
from elasticsearch import Elasticsearch
from elasticsearch import helpers

def get_md5(url):
    if isinstance(url, str):
        url = url.encode("utf-8")
    m = hashlib.md5()
    m.update(url)
    return m.hexdigest()



def post_to_es(name,qlist):
    es = Elasticsearch('http://172.17.15.65:9208')

    index_name = name + datetime.datetime.now().strftime('%Y-%m')
    es.indices.create(index=index_name, ignore=400)
    if not es.indices.get_mapping(index=index_name, doc_type='audiovisual_outsite'):
        es.indices.put_mapping(index=index_name, doc_type='audiovisual_outsite', body={
            "_all": {"enabled": True},
            "properties": {
                "program_name": {"analyzer": "ik_smart", "term_vector": "with_positions_offsets", "type": "string"},
                'source': {"index": "not_analyzed", "type": "string"},
                "'video_url'": {"index": "not_analyzed", "type": "string"},
                "imgurl": {"index": "not_analyzed", "type": "string"},
                "duration": {"index": "not_analyzed", "type": "string"},
                "publish_time": {"format": "epoch_second", "type": "date"},
                "create_time": {"format": "epoch_second", "type": "date"},
                "tag": {"analyzer": "ik_smart", "term_vector": "with_positions_offsets", "type": "string"}
            }
        })
    return index_name,helpers.bulk(es, qlist)


def get_word_from_redis():
    a = sys.argv[1]
    q = redis.Redis(host='172.17.13.254', port='6379', password="Bohui#123", db=9)


    try:
        word = q.get('audio_visual:outsite_search:' + str(a)).decode('utf-8')
    except:
        word = ''
    return word




user_agnet_list = [

	    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
	    "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
	    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
	    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
	    "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",
	    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
	    "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",
	    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
	    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
	    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
	    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
	    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
	    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
	    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
	    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
	    "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",
	    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
	    "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
	    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10",
	    "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8",
	    "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5",
	    "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.1.17) Gecko/20110123 (like Firefox/3.x) SeaMonkey/2.0.12",
	    "Mozilla/5.0 (Windows NT 5.2; rv:10.0.1) Gecko/20100101 Firefox/10.0.1 SeaMonkey/2.7.1",
	    "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; en-US) AppleWebKit/532.8 (KHTML, like Gecko) Chrome/4.0.302.2 Safari/532.8",
	    "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_4; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Chrome/6.0.464.0 Safari/534.3",
	    "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.15 Safari/534.13",
	    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.186 Safari/535.1",
	    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.54 Safari/535.2",
	    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7",
	    "Mozilla/5.0 (Macintosh; U; Mac OS X Mach-O; en-US; rv:2.0a) Gecko/20040614 Firefox/3.0.0 ",
	    "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.0.3) Gecko/2008092414 Firefox/3.0.3",
	    "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1) Gecko/20090624 Firefox/3.5",
	    "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 AlexaToolbar/alxf-2.0 Firefox/3.6.14",
	    "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15",
	    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1"

]

def choice_useragent():
    return random.choice(user_agnet_list)
weibo_cookie_List =[
{'SCF=AsUEur6oC8CkAP5FwvgQwu6q7_5czirBNByDOzcmukfIe9hzvDpQM-X0LuAvgFaZKbK2BYEWmPAjubbfbsHHPGg.;ALF=1512803306;SUHB=0tKqeE169pFMQG;SSOLoginState=1510211308;SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WWYm-ByiUpWEj9_XqOUNyDm5JpX5K-hUgL.FoqXeh-7e0nNSoz2dJLoIppQ9g8EUsnLxK-LB--LBoeLxKMLB.-L1--peKqf;SUB=_2A253B468DeRhGeBK61cR8ybLzT6IHXVUCxL0rDV6PUJbktAKLU7RkW0yyH-gwNKV7C1oZTeS1DUR2xcoOg..;_T_WM=b7fa22ace35e98c9efb5302f13c926c5'},
{'SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9W5m9IeBsCqSo.2lJsg_4MNn5JpX5K-hUgL.FoqXeh-7eoBEShq2dJLoIEM7SK-fSCH81CHFeb-ReCH8Sb-41CHFBCH8SbHFSEHFePWE9gSL9s-t;SCF=AhMcETW3EH-qSa-A_5NOGUZ3RhzdcA5PEFfv7k1xgFiqaa1UntZRMOkkG-Ip9A_XVnnkZrWIrmi8awxCopvqqaU.;SSOLoginState=1510210666;SUHB=0u5RzkGfPeqkKV;ALF=1512802663;SUB=_2A253B4w5DeRhGeBK61cR8irOzzqIHXVUCxRxrDV6PUJbktAKLRP-kW0ePPQrendt2CzA66DicsP02Km38g..;_T_WM=6e9f47ccc280692de894d7e7a2834401'},
]


def choice_cookie():

    r = requests.get('http://127.0.0.1:5000/weibo/random')
    q = []
    for k, v in r.json().items():
        b = (k + '=' + v)
        q.append(b)
    cookie = ';'.join(q)
    return cookie


if __name__ == "__main__":
    print (choice_cookie())
